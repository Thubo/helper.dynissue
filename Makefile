# Variables
PWD = $(shell pwd)
RM = $(shell which rm) -rf

all: uninstall install

help:
	@echo "Creates the symlinks for"
	@echo "  /sbin/ifup-local"
	@echo "  /sbin/update-issue"

# uninstall
uninstall:
	sudo $(RM) "/sbin/ifup-local"
	sudo $(RM) "/sbin/update-issue"

# install
install:
	sudo ln -s "$(PWD)/ifup-local.sh" "/sbin/ifup-local"
	sudo ln -s "$(PWD)/update-issue.sh" "/sbin/update-issue"
