#!/bin/sh
###############################################################################
# The MIT License (MIT)
#
# Copyright (c) [2015] [Matthias Thubauville]
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
###############################################################################
# Author:
#  Matthias Thubauville
#
# Description:
#  Call the script, which creates the content and puts the output in /etc/issue
#  We have to do it asynchronously because at the time this script is called, it
#  does not know the ip address yet.
#  This file is meant to be accessed via /sbin/ifup-local
#  Works only in combination with the /sbin/update-issue script
###############################################################################

if [[ -x /sbin/update-issue ]]; then
  sleep 5s && /sbin/update-issue > /etc/issue
fi
